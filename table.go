package models

type Table struct {
	ID       string   `json:"id"`
	Status   string   `json:"status"`
	Players  []Player `json:"player"`
	Address  string   `json:"address"`
	VPlayers []string `json:"vplayers"` //Visible players only for screen purposes
	SeatNum  int      `json:"seatnum"`
	Decks    int      `json:"decks"` //Number of starting decks
}
